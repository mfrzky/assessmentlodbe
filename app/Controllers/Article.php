<?php
 
namespace App\Controllers;
 
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\ArticleModel;
 
class Article extends ResourceController {
    use ResponseTrait;
    
    public function index() {
        $model = new ArticleModel();
        $data['article'] = $model->orderBy('id', 'DESC')->findAll();
        return $this->respond($data, 200);
    }

    public function show($id = null) {
        $model = new ArticleModel();
        $data = $model->where('id', $id)->first();
        if ($data) {
            return $this->respond($data);
        } else {
            return $this->failNotFound('Data tidak ditemukan.');
        }
    }

    public function create() {
        if(!$this->validate([
			'title'	   	=> 'required',
			'author'	=> 'required',
			'content'	=> 'required',
		])) {
			return $this->response->setJSON(['data' => null, "message" => \Config\Services::validation()->getErrors()])->setStatusCode(400);
		}

        $model = new ArticleModel();
		$data = [
            'title' => $this->request->getVar('title'),
            'author'  => $this->request->getVar('author'),
            'content'  => $this->request->getVar('content'),
        ];
        $model->insert($data);
        
        $response = [
            'status'   => 201,
            'error'    => null,
            'messages' => [
                'success' => 'Data produk berhasil ditambahkan.'
            ]
        ];
		
		return $this->setResponseFormat('json')->respondCreated($response);
    }

    public function update($id = null)  {
        if(!$this->validate([
			'title'	   	=> 'required',
			'author'	=> 'required',
			'content'	=> 'required',
		])) {
			return $this->response->setJSON(['data' => null, "message" => \Config\Services::validation()->getErrors()])->setStatusCode(400);
		}
        
        $model = new ArticleModel();

        $data = [
            'title' => $this->request->getVar('title'),
            'author'  => $this->request->getVar('author'),
            'content'  => $this->request->getVar('content'),
        ];
        $model->update($id, $data);
        
        $response = [
            'status'   => 200,
            'error'    => null,
            'messages' => [
                'success' => 'Data produk berhasil diubah.'
            ]
        ];

        return $this->respond($response);
    }

    public function delete($id = null) {
        $model = new ArticleModel();
        $data = $model->where('id', $id)->delete($id);
        if ($data) {
            $model->delete($id);
            $response = [
                'status'   => 200,
                'error'    => null,
                'messages' => [
                    'success' => 'Data produk berhasil dihapus.'
                ]
            ];
            return $this->respondDeleted($response);
        } else {
            return $this->failNotFound('Data tidak ditemukan.');
        }
    }
}