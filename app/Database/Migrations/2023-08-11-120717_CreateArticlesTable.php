<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateArticlesTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
			'id' => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
				'auto_increment' => true
			],
			'title' => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
				'null'           => false,
			],
			'author' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
				'null'           => false,
			],
			'content' => [
				'type'           => 'TEXT',
				'null'           => false,
			]
		]);

		$this->forge->addKey('id', TRUE);
		$this->forge->createTable('articles');
    }

    public function down()
    {
        $this->forge->dropTable('articles');
    }
}
